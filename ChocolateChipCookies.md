### Chocolate Chip Cookies

# Ingredients
- 1/2 cup (1 stick) Butter, softened 🧈 
- 1 cup dark brown sugar
- 3 tbsp turbinado sugar
- 1 large egg 🥚 
- 3 tsp vanilla extract
- 1 3/4 cups all purpose flour
- 1/2 tsp baking powder
- 1/2 tsp baking soda
- 1/2 tsp salt
- 1 1/2 tsp instant espresso powder ☕️  (Sub with a shot of espresso if you know what's up)
- 1 1/2 cups semisweet chocolate chips 🍫 
- 1 oz Bourbon 🥃 *optional* (Because there are no rules)

# Instructions
- Preheat oven to 300F
- In a small saucepan, brown half of the butter over medium heat (about 2 minutes)
- Cream together butter and sugars in a large mixing bowl until fluffy
- Add in egg, vanilla, and espresso powder and beat until smooth
- Sift together flour, baking powder, baking soda, and salt in a seperate bowl
- Add flour mixture and mix thouroughly
- Add chocolate chips and fold into dough (*add your bourbon here to coat the chocolate chips*)
- Bake at 300F for 20 minutes or until lightly browned.  For crispy cookies, bake 3-5 mins longer.
- Let cool for a few minutes (*patience young one*).
- Eat one. I'm proud of you and you deserve it.

Based off of NM Chocolate Chip Cookies
